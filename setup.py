from setuptools import setup

setup(
    name='py-zed-recorder',
    version='0.1',
    description='',
    long_description=open('README.md').read(),
    url='https://gitlab.com/neural-stereo-tools/py-zed-recorder',
    author='Alexander Frenzel',
    author_email='alex@relatedworks.com',
    license='BSD',
    packages=['zed_recorder'],
    install_requires=[
        'click==6.7',
        'numpy==1.13.1',
        'opencv-python==3.3.0.10',
        'runstats==1.7.1',
    ],
    extras_require={
        'dev': [
            'flake8==3.4.1',
            'flake8-isort==2.2.2',
            'isort==4.2.15',
            'ipython==6.1.0',
            'pdbpp==0.9.1',
            'mccabe==0.6.1',
            'mypy==0.521',
            'pytest==3.2.2',
            'pytest-flake8==0.8.1',
            'pytest-mypy==0.3.0',
            'pytest-sugar==0.9.0',
        ],
    },
    entry_points={
        'console_scripts': [
            'zed_grab=zed_recorder.cli:simple_grab',
            'zed_multi_grab=zed_recorder.cli:multi_grab',
            'zed_live=zed_recorder.cli:live',
        ],
    },
    include_package_data=True,
    classifiers=[
        'Development Status :: 4 - Beta',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
    ],
)
