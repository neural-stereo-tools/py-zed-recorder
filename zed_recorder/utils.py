import enum
from typing import Any, Generator

import click


class EnumType(click.Choice):
    def __init__(self, enum: enum.Enum) -> None:
        self.enum = enum
        choices = list(self.__get_choices())
        super().__init__(choices)

    def convert(self, *args, **kwargs):
        name = super().convert(*args, **kwargs)
        return self.__get_choice(name)

    def __get_choices(self) -> Generator[str, None, None]:
        for choice in self.enum:
            name = choice.name
            if name.lower().endswith('_last'):
                continue
            name = name.replace(f'{self.enum.__name__}_', '')
            if name.lower() == 'none':
                continue
            yield name

    def __get_choice(self, name: str):
        name = f'{self.enum.__name__}_{name}'
        return self.enum[name]


def filter_zed_enum(enum: enum.Enum) -> Generator[Any, None, None]:
    for option in enum:
        if '_LAST' in option.name:
            continue
        if 'NONE' in option.name:
            continue
        yield option


def zed_enum_to_str(value) -> str:
    name = value.name
    name = name.replace(f'{value.__class__.__name__}_', '')
    name = name.lower()
    return name
