import cv2
import numpy as np
import pyzed.camera as zcam
import pyzed.core as core
import pyzed.defines as sl
import pyzed.types as tp
from runstats import Statistics

from .camera import ZEDCamera


def print_settings(zed):
    print(
        zed.get_camera_settings(sl.PyCAMERA_SETTINGS.PyCAMERA_SETTINGS_BRIGHTNESS),
        zed.get_camera_settings(sl.PyCAMERA_SETTINGS.PyCAMERA_SETTINGS_CONTRAST),
        zed.get_camera_settings(sl.PyCAMERA_SETTINGS.PyCAMERA_SETTINGS_HUE),
        zed.get_camera_settings(sl.PyCAMERA_SETTINGS.PyCAMERA_SETTINGS_SATURATION),
        zed.get_camera_settings(sl.PyCAMERA_SETTINGS.PyCAMERA_SETTINGS_GAIN),
        zed.get_camera_settings(sl.PyCAMERA_SETTINGS.PyCAMERA_SETTINGS_EXPOSURE),
        zed.get_camera_settings(sl.PyCAMERA_SETTINGS.PyCAMERA_SETTINGS_WHITEBALANCE),
    )


def preview(**params):
    init_params = zcam.PyInitParameters()
    init_params.camera_resolution = params['resolution']
    init_params.depth_mode = params['depth_mode']
    init_params.coordinate_units = sl.PyUNIT.PyUNIT_MILLIMETER

    runtime_params = zcam.PyRuntimeParameters()
    runtime_params.enable_depth = True
    runtime_params.sensing_mode = params['sensing_mode']

    stats_disp = Statistics()
    stats_depth = Statistics()
    image = core.PyMat()
    disp = core.PyMat()
    disp_img = core.PyMat()
    depth = core.PyMat()
    key = ''

    counter = 0
    with ZEDCamera(init_params) as zed:
        has_limit = bool(params['limit'])
        while key != 113:  # for 'q' key
            counter += 1
            if has_limit:
                if counter - params['skip'] > params['limit']:
                    print(f'stopped before frame {counter}')
                    break
            if zed.grab(runtime_params) == tp.PyERROR_CODE.PySUCCESS:
                print_settings(zed)
                if params['skip'] and params['skip'] >= counter:
                    print(f'skipping frame {counter}')
                    continue
                zed.retrieve_image(image, sl.PyVIEW.PyVIEW_LEFT)
                zed.retrieve_image(disp_img, sl.PyVIEW.PyVIEW_DEPTH)
                zed.retrieve_measure(disp, sl.PyMEASURE.PyMEASURE_DISPARITY)
                zed.retrieve_measure(depth, sl.PyMEASURE.PyMEASURE_DEPTH)

                height, width, _ = image.get_data().shape
                center_y = int(height / 2)
                center_x = int(width / 2)

                center_depth = depth.get_data()[center_y][center_x]
                center_disp = disp.get_data()[center_y][center_x]
                print(f"max(disp)={disp.get_data().max()}, min(disp)={disp.get_data().min()}")  # NOQA
                print(f"max(depth)={depth.get_data().max()}, min(depth)={depth.get_data().min()}")  # NOQA
                print(f"({center_y},{center_x}): depth={center_depth}, disp={center_disp}.")  # NOQA
                stats_disp.push(center_disp)
                stats_depth.push(center_depth)

                def mark_point(img, y, x, color=(0, 0, 255, 255), length=10):
                    for i in range(-length, length):
                        img[y + i, x + i] = color
                        img[y + i, x - i] = color

                # prepare and display
                vis_img = image.get_data()
                vis_disp = disp_img.get_data()
                mark_point(vis_img, center_y, center_x)
                vis = np.concatenate((vis_img, vis_disp), axis=0)
                vis = cv2.resize(vis, (0, 0), fx=0.5, fy=0.5)
                cv2.imshow("ZED", vis)
            key = cv2.waitKey(5)
    print(f'''
Statistics
=========
depth
-----
min: {stats_depth.minimum()}
max: {stats_depth.maximum()}
mean: {stats_depth.mean()}
std: {stats_depth.stddev()}
var: {stats_depth.variance()}

disp
-----
min: {stats_disp.minimum()}
max: {stats_disp.maximum()}
mean: {stats_disp.mean()}
std: {stats_disp.stddev()}
var: {stats_disp.variance()}
    ''')
    cv2.destroyAllWindows()
