import pyzed.camera as zcam
import pyzed.defines as sl
import pyzed.types as tp

from .utils import filter_zed_enum


class CameraError(Exception):
    pass


class ZEDCamera(zcam.PyZEDCamera):
    def __init__(self, params: zcam.PyInitParameters) -> None:
        self.params = params
        super().__init__()

    def __enter__(self):
        err = self.open(self.params)
        if err != tp.PyERROR_CODE.PySUCCESS:
            raise CameraError(str(err))
        return self

    def __exit__(self, *args, **kwargs):
        self.close()


RESOLUTIONS = list(filter_zed_enum(sl.PyRESOLUTION))
DEPTH_MODES = list(filter_zed_enum(sl.PyDEPTH_MODE))
SENSING_MODES = list(filter_zed_enum(sl.PySENSING_MODE))
