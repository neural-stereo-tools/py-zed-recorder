import os
from pathlib import Path

import click
import pyzed.defines as sl

from . import camera, utils
from .live import preview
from .record import record_images


@click.command()
@click.option('--resolution',
              type=utils.EnumType(sl.PyRESOLUTION),
              default='HD720')
@click.option('--depth_mode',
              type=utils.EnumType(sl.PyDEPTH_MODE),
              default='PERFORMANCE')
@click.option('--sensing_mode',
              type=utils.EnumType(sl.PySENSING_MODE),
              default='STANDARD')
@click.option('--skip_frames', type=int, default=2)
@click.option('--frames', type=int, default=1)
@click.argument('output_dir', type=click.Path(exists=True))
def simple_grab(output_dir, **params) -> None:
    output_dir = Path(output_dir)
    record_images(output_dir, **params)


@click.command()
@click.option('--skip_frames', type=int, default=2)
@click.option('--frames', type=int, default=1)
@click.argument('comment', type=str)
@click.argument('distance', type=float)
@click.argument('output_dir', type=click.Path(exists=True))
def multi_grab(comment: str, distance: float, output_dir: str,
               **options):
    output_dir = Path(output_dir)
    for resolution in camera.RESOLUTIONS:
        for dmode in camera.DEPTH_MODES:
            for smode in camera.SENSING_MODES:
                target_dir = output_dir
                target_dir /= comment
                target_dir /= str(distance)
                target_dir /= utils.zed_enum_to_str(resolution)
                target_dir /= utils.zed_enum_to_str(dmode)
                target_dir /= utils.zed_enum_to_str(smode)
                os.makedirs(target_dir, exist_ok=True)

                record_images(
                    target_dir,
                    resolution=resolution,
                    depth_mode=dmode,
                    sensing_mode=smode,
                    **options,
                )


@click.command()
@click.option('--resolution',
              type=utils.EnumType(sl.PyRESOLUTION),
              default='HD720')
@click.option('--depth_mode',
              type=utils.EnumType(sl.PyDEPTH_MODE),
              default='PERFORMANCE')
@click.option('--sensing_mode',
              type=utils.EnumType(sl.PySENSING_MODE),
              default='STANDARD')
@click.option('--skip', type=int, default=2)
@click.option('--limit', type=int)
def live(**params) -> None:
    preview(**params)
