import json
import logging
from pathlib import Path

import cv2
import pyzed.camera as zcam
import pyzed.core as core
import pyzed.defines as sl
import pyzed.types as tp

from .camera import ZEDCamera

logger = logging.getLogger(__name__)


def camera_params_to_dict(params: core.PyCameraParameters):
    data = {}
    for prop in dir(params):
        if prop.startswith('__'):
            continue
        data[prop] = getattr(params, prop)
    return data


def calib_params_to_dict(params: core.PyCalibrationParameters):
    return {
        'T': params.T.tolist(),
        'R': params.R.tolist(),
        'left_cam': camera_params_to_dict(params.left_cam),
        'right_cam': camera_params_to_dict(params.right_cam),
    }


def camera_info_to_dict(params: core.PyCameraInformation):
    return {
        'calibration_parameters': calib_params_to_dict(params.calibration_parameters),  # NOQA
        'calibration_parameters_raw': calib_params_to_dict(params.calibration_parameters_raw),  # NOQA
        'firmware_version': params.firmware_version,
        'serial_numer': params.serial_number,
    }


def record_images(output_dir: Path, **params) -> None:
    init_params = zcam.PyInitParameters()
    init_params.camera_resolution = params['resolution']
    init_params.depth_mode = params['depth_mode']

    # TODO
    init_params.camera_disable_self_calib = True
    init_params.camera_fps = 30  # Set fps at 30

    runtime_params = zcam.PyRuntimeParameters()
    runtime_params.enable_depth = True
    # runtime_params.enable_point_cloud = True
    runtime_params.sensing_mode = params['sensing_mode']

    images = {view: core.PyMat()
              for view in sl.PyVIEW
              if ('last' not in view.name.lower() and
                  'normals_right' not in view.name.lower() and
                  'depth' not in view.name.lower() and
                  'gray' not in view.name.lower())}
    depth = core.PyMat()
    disparity = core.PyMat()

    global_counter = 0
    counter = 0
    parent_output_dir = output_dir
    with ZEDCamera(init_params) as zed:
        while True:
            if zed.grab(runtime_params) != tp.PyERROR_CODE.PySUCCESS:
                raise RuntimeError('Could not grab images')

            global_counter += 1
            if global_counter <= int(params.get('skip_frames', 1)):
                print(f'skipping frame {global_counter}')
                continue

            if counter >= int(params.get('frames', 1)):
                break

            counter += 1
            output_dir = parent_output_dir / str(counter)
            output_dir.mkdir(parents=True, exist_ok=True)
            print(f'storing image into {output_dir}')

            camera_info = zed.get_camera_information()
            calib_state = zed.get_self_calibration_state()

            # A new image is available if grab() returns PySUCCESS
            time = zed.get_camera_timestamp()
            for view, image in images.items():
                zed.retrieve_image(image, view)

            zed.retrieve_measure(depth, sl.PyMEASURE.PyMEASURE_DEPTH)
            zed.retrieve_measure(disparity, sl.PyMEASURE.PyMEASURE_DISPARITY)

            # store point cloud
            zcam.save_camera_point_cloud_as(
                zed, sl.PyPOINT_CLOUD_FORMAT.PyPOINT_CLOUD_FORMAT_PLY_ASCII,
                str(output_dir / 'pointcloud.ply'),
            )

            # store depth
            zcam.save_mat_depth_as(
                depth, sl.PyDEPTH_FORMAT.PyDEPTH_FORMAT_PFM,
                str(output_dir / f'depth.pfm'),
            )
            zcam.save_mat_depth_as(
                disparity, sl.PyDEPTH_FORMAT.PyDEPTH_FORMAT_PFM,
                str(output_dir / f'disparity.pfm'),
            )

            # store images
            for view, image in images.items():
                viewname = view.name.replace('PyVIEW_', '').lower()
                image = image.get_data()  # convert to numpy array
                # lossless webp!
                cv2.imwrite(
                    str(output_dir / f'{viewname}.webp'),
                    image,
                    [cv2.IMWRITE_WEBP_QUALITY, 110],
                )

            # store config
            # convert enums to str
            params = {key: str(value) for key, value in params.items()}
            params['time'] = time
            params['self_calib_state'] = calib_state.name
            with open(output_dir / 'config.json', mode='w') as f:
                json.dump(params, f)

            init_params.save(str(output_dir / 'init_params'))
            runtime_params.save(str(output_dir / 'runtime_params'))

            with open(output_dir / 'camera.json', mode='w') as f:
                json.dump(camera_info_to_dict(camera_info), f)

            # store additional configurations
            with open(output_dir / 'camera_config.json', mode='w') as f:
                json.dump({
                    'brightness': zed.get_camera_settings(sl.PyCAMERA_SETTINGS.PyCAMERA_SETTINGS_BRIGHTNESS),  # NOQA
                    'contrast': zed.get_camera_settings(sl.PyCAMERA_SETTINGS.PyCAMERA_SETTINGS_CONTRAST),  # NOQA
                    'hue': zed.get_camera_settings(sl.PyCAMERA_SETTINGS.PyCAMERA_SETTINGS_HUE),  # NOQA
                    'saturation': zed.get_camera_settings(sl.PyCAMERA_SETTINGS.PyCAMERA_SETTINGS_SATURATION),  # NOQA
                    'gain': zed.get_camera_settings(sl.PyCAMERA_SETTINGS.PyCAMERA_SETTINGS_GAIN),  # NOQA
                    'exposure': zed.get_camera_settings(sl.PyCAMERA_SETTINGS.PyCAMERA_SETTINGS_EXPOSURE),  # NOQA
                    'whitebalance': zed.get_camera_settings(sl.PyCAMERA_SETTINGS.PyCAMERA_SETTINGS_WHITEBALANCE),  # NOQA
                }, f)
