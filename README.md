# py-zed-recorder

## Requirements

- CUDA
- ZED SDK 2.1
- Python 3.6.x


## Installation

Run the following commands to install `py-zed-recorder`.

        pip install git+https://gitlab.com:neural-stereo-tools/py-zed-recorder.git#egg=zed_recorder
        pip install cython
        pip install git+https://gitlab.com/neural-stereo-tools/zed-python.git#egg=pyzed

It is recommend to run these command in an virtual environment!


## Usage

There are 3 tools available: `zed_grab`, `zed_multi_grab` and `zed_live`.
Use `--help` to see the usage information.


### zed_grab

This simple tool grabs one or more image from the ZED camera. The following
files will be created.

        camera_config.json                ...contrast, brightness, ...
        camera.json                       ...camera calibration
        confidence.webp
        config.json                       ...parameters passed to `zed_grab`
        depth.pfm
        disparity.pfm
        init_params_ZED_Param.yml         ...internal zed parameters
        left_unrectified.webp
        left.webp
        normals.webp
        pointcloud.ply
        right_unrectified.webp
        right.webp
        runtime_params_ZED_Param.yml      ...internal zed parameters
        side_by_side.webp


### zed_multi_grab

Grabs images for all possible configurations (resolution, depth_mode,
sensing_mode). The resulting directory structure looks like

        <COMMENT>/<DISTANCE>/hd2k/performance/standard/1
        <COMMENT>/<DISTANCE>/hd2k/performance/fill/1
        <COMMENT>/<DISTANCE>/hd2k/medium/standard/1
        <COMMENT>/<DISTANCE>/hd2k/medium/fill/1
        <COMMENT>/<DISTANCE>/hd2k/quality/standard/1
        <COMMENT>/<DISTANCE>/hd2k/quality/fill/1
        <COMMENT>/<DISTANCE>/hd1080/performance/standard/1
        <COMMENT>/<DISTANCE>/hd1080/performance/fill/1
        <COMMENT>/<DISTANCE>/hd1080/medium/standard/1
        <COMMENT>/<DISTANCE>/hd1080/medium/fill/1
        <COMMENT>/<DISTANCE>/hd1080/quality/standard/1
        <COMMENT>/<DISTANCE>/hd1080/quality/fill/1
        <COMMENT>/<DISTANCE>/hd720/performance/standard/1
        <COMMENT>/<DISTANCE>/hd720/performance/fill/1
        <COMMENT>/<DISTANCE>/hd720/medium/standard/1
        <COMMENT>/<DISTANCE>/hd720/medium/fill/1
        <COMMENT>/<DISTANCE>/hd720/quality/standard/1
        <COMMENT>/<DISTANCE>/hd720/quality/fill/1
        <COMMENT>/<DISTANCE>/vga/performance/standard/1
        <COMMENT>/<DISTANCE>/vga/performance/fill/1
        <COMMENT>/<DISTANCE>/vga/medium/standard/1
        <COMMENT>/<DISTANCE>/vga/medium/fill/1
        <COMMENT>/<DISTANCE>/vga/quality/standard/1
        <COMMENT>/<DISTANCE>/vga/quality/fill/1


### zed_live

Opens a windows and shows the disparity map and the view of the left camera.
